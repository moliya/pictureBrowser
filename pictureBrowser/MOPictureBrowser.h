//
//  MOPictureBrowser.h
//  pictureBrowser
//
//  Created by moliya on 15/3/25.
//  Copyright (c) 2015年 Wanquan Network Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureView : UIScrollView

/**
 *  @author carefree
 *
 *  @brief  设置图片
 */
- (void)setImage:(UIImage *)image;
/**
 *  @author carefree
 *
 *  @brief  设置起始的大小
 */
- (void)setOriginFrame:(CGRect)frame;

/**
 *  @author carefree
 *
 *  @brief  图片全屏显示
 */
- (void)showFullScreen;
/**
 *  @author carefree
 *
 *  @brief  图片缩小还原
 */
- (void)revertBack;

@end

//************************************************/

typedef void(^DeletePictureBlock)(NSUInteger index);
typedef void(^DismissBlock)();

@interface MOPictureBrowser : UIView

//当前选择的图片数组下标
@property (nonatomic,assign) NSUInteger         currentIndex;
//图片数组
@property (nonatomic,strong) NSMutableArray     *pictures;

/**
 *  @author carefree
 *
 *  @brief  设置图片数组
 */
- (void)addPictures:(NSArray *)pics;

/**
 *  @author carefree
 *
 *  @brief  显示
 */
- (void)showFromView:(UIView *)view;
- (void)showFromView:(UIView *)view deleteBlock:(DeletePictureBlock)block;
- (void)showFromView:(UIView *)view deleteBlock:(DeletePictureBlock)block dismissBlock:(DismissBlock)blockB;

/**
 *  @author carefree
 *
 *  @brief  设置删除的回调
 */
- (void)setDeleteBlock:(DeletePictureBlock)deleteBlock;
/**
 *  @author carefree
 *
 *  @brief  设置消失的回调
 */
- (void)setDismissBlock:(DismissBlock)dismissBlock;

@end
