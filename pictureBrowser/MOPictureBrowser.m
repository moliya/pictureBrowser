//
//  MOPictureBrowser.m
//  pictureBrowser
//
//  Created by moliya on 15/3/25.
//  Copyright (c) 2015年 Wanquan Network Technology. All rights reserved.
//

#import "MOPictureBrowser.h"

//获取设备的物理宽度
#define ScreenWidth [UIScreen mainScreen].bounds.size.width

@interface PictureView ()<UIScrollViewDelegate>

@property (nonatomic,strong) UIImageView    *imageView;
//起始的坐标
@property (nonatomic,assign) CGRect         originRect;
//全屏显示的坐标
@property (nonatomic,assign) CGRect         displayRect;

@end

@implementation PictureView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupPictureView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupPictureView];
    }
    return self;
}

/**
 *  @author carefree
 *
 *  @brief  初始化设置
 */
- (void)setupPictureView {
    //imageView设置
    _imageView = [[UIImageView alloc] init];
    _imageView.clipsToBounds = YES;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:_imageView];
    //scrollView代理
    self.delegate = self;
    //关闭滚动条显示
    [self setShowsHorizontalScrollIndicator:NO];
    [self setShowsVerticalScrollIndicator:NO];
    [self setDelaysContentTouches:NO];
}

#pragma mark - public
- (void)setImage:(UIImage *)image {
    if (image)
    {
        _imageView.image = image;
        CGSize imgSize = image.size;
        
        //判断首先缩放的值
        float scaleX = self.frame.size.width/imgSize.width;
        float scaleY = self.frame.size.height/imgSize.height;
        
        //倍数小的，先到边缘
        
        if (scaleX > scaleY)
        {
            //Y方向先到边缘
            float _imageViewWidth = imgSize.width*scaleY;
            self.maximumZoomScale = self.frame.size.width/_imageViewWidth;
            
            _displayRect = (CGRect){self.frame.size.width/2-_imageViewWidth/2,0,_imageViewWidth,self.frame.size.height};
        }
        else
        {
            //X先到边缘
            float _imageViewHeight = imgSize.height*scaleX;
            self.maximumZoomScale = self.frame.size.height/_imageViewHeight;
            
            _displayRect = (CGRect){0,self.frame.size.height/2-_imageViewHeight/2,self.frame.size.width,_imageViewHeight};
        }
    }
}

- (void)setOriginFrame:(CGRect)frame {
    _imageView.frame = frame;
    _originRect = frame;
}

- (void)showFullScreen {
    _imageView.frame = _displayRect;
}

- (void)revertBack {
    _imageView.frame = _originRect;
}


#pragma mark - scroll delegate
//缩放view
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGSize boundsSize = scrollView.bounds.size;
    CGRect imgFrame = _imageView.frame;
    CGSize contentSize = scrollView.contentSize;
    
    CGPoint centerPoint = CGPointMake(contentSize.width/2, contentSize.height/2);
    
    // center horizontally
    if (imgFrame.size.width <= boundsSize.width)
    {
        centerPoint.x = boundsSize.width/2;
    }
    
    // center vertically
    if (imgFrame.size.height <= boundsSize.height)
    {
        centerPoint.y = boundsSize.height/2;
    }
    
    _imageView.center = centerPoint;
}
/**
 *  @author carefree
 *
 *  @brief  图片缩放
 */
- (void)doubleTappedOnPicture {
    [UIView animateWithDuration:0.4 animations:^{
        CGFloat scale =  self.zoomScale == 1.0 ? self.maximumZoomScale : 1.0f;
        self.zoomScale = scale;
    }];
    
}

@end

@interface MOPictureBrowser ()<UIScrollViewDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) UIScrollView   *pageScrollView;
@property (nonatomic,strong) NSMutableArray *pictureViews;
@property (nonatomic,copy) DeletePictureBlock   deleteBlock;
@property (nonatomic,copy) DismissBlock         dismissBlock;
@end

@implementation MOPictureBrowser

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupBrowser];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupBrowser];
    }
    return self;
}

/**
 *  @author carefree
 *
 *  @brief  初始化设置
 */
- (void)setupBrowser {
    //黑色背景
    [self setBackgroundColor:[UIColor blackColor]];
    //长按手势,用于删除
    UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAlert:)];
    //单击手势，用于隐藏
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
    //双击手势，用于缩放
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped)];
    doubleTap.numberOfTapsRequired = 2;
    //解决单双击冲突，当不是双击时才触发单击
    [tap requireGestureRecognizerToFail:doubleTap];
    
    [self addGestureRecognizer:press];
    [self addGestureRecognizer:tap];
    [self addGestureRecognizer:doubleTap];
    
    _currentIndex = 0;
    _pictures = [NSMutableArray array];
    _pictureViews = [NSMutableArray array];
}

#pragma mark - public
- (void)showFromView:(UIView *)view {
    [self showFromView:view deleteBlock:nil dismissBlock:nil];
}

- (void)showFromView:(UIView *)view deleteBlock:(DeletePictureBlock)block {
    [self showFromView:view deleteBlock:block dismissBlock:nil];
}

- (void)showFromView:(UIView *)view deleteBlock:(DeletePictureBlock)block dismissBlock:(DismissBlock)blockB {
    //是否有图片
    if (_pictures.count <= 0) {
        return;
    }
    _deleteBlock = block;
    _dismissBlock = blockB;
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:self];
    
    CGRect fromRect = [view.superview convertRect:view.frame toView:window];
    
    _pageScrollView = [[UIScrollView alloc] initWithFrame:self.frame];
    _pageScrollView.pagingEnabled = YES;
    _pageScrollView.delegate = self;
    _pageScrollView.contentSize = CGSizeMake(ScreenWidth * _pictures.count, _pageScrollView.bounds.size.height);
    
    CGPoint contentOffset = _pageScrollView.contentOffset;
    contentOffset.x = _currentIndex * _pageScrollView.bounds.size.width;
    _pageScrollView.contentOffset = contentOffset;
    
    //将图片添加到scrollView
    for (UIImage *image in _pictures) {
        PictureView *picture = [[PictureView alloc] init];
        
        NSUInteger index = [_pictures indexOfObject:image];
        if (_currentIndex == index) {
            picture.frame = (CGRect){contentOffset,_pageScrollView.bounds.size};
            [picture setImage:image];
            [picture setOriginFrame:fromRect];
        }else {
            picture.frame = (CGRect){index * _pageScrollView.bounds.size.width,0,_pageScrollView.bounds.size};
            [picture setImage:image];
            [picture showFullScreen];
        }
        [_pictureViews addObject:picture];
        [_pageScrollView addSubview:picture];
    }
    [self addSubview:_pageScrollView];
    
    //放大动画
    [UIView animateWithDuration:0.5 animations:^{
        PictureView *picture = _pictureViews[_currentIndex];
        if ([picture respondsToSelector:@selector(showFullScreen)]) {
            [picture showFullScreen];
        }
    }];
}

- (void)setDeleteBlock:(DeletePictureBlock)deleteBlock {
    _deleteBlock = deleteBlock;
}

- (void)setDismissBlock:(DismissBlock)dismissBlock {
    _dismissBlock = dismissBlock;
}

- (void)addPictures:(NSArray *)pics {
    [_pictures addObjectsFromArray:pics];
}

- (void)setPictures:(NSArray *)pictures {
    [_pictures addObjectsFromArray:pictures];
}

#pragma mark - scrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    _currentIndex = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

#pragma mark - alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        //点击确定，删除图片
        [self deleteCurrentPicture];
    }
}

#pragma mark - private
/**
 *  @author carefree
 *
 *  @brief  删除当前图片
 */
- (void)deleteCurrentPicture {
    [UIView animateWithDuration:0.5 animations:^{
        //移除图片
        [_pictureViews[_currentIndex] removeFromSuperview];
        //重新设置该图片后面的图片的frame
        for (NSUInteger i = _currentIndex + 1; i < _pictureViews.count; i ++) {
            UIView *subview = _pictureViews[i];
            CGRect frame = subview.frame;
            //x轴向左移scrollView的宽度
            frame.origin.x -= _pageScrollView.bounds.size.width;
            [subview setFrame:frame];
        }
        //重新设置scrollView的contentSize
        CGSize size = _pageScrollView.contentSize;
        size.width -= _pageScrollView.bounds.size.width;
        _pageScrollView.contentSize = size;
    } completion:^(BOOL finished) {
        //移除对应的数据
        [_pictureViews removeObjectAtIndex:_currentIndex];
        [_pictures removeObjectAtIndex:_currentIndex];
        //主动调用
        [self scrollViewDidEndDecelerating:_pageScrollView];
        //执行回调
        if (_deleteBlock) {
            _deleteBlock(_currentIndex);
        }
        //当没有图片时，则隐藏
        if (_pictures.count <= 0) {
            [self hide];
        }
    }];
}
/**
 *  @author carefree
 *
 *  @brief  显示删除提示框
 *
 *  @param recognizer 手势
 */
- (void)deleteAlert:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"删除这张图片?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }
}
//双击
- (void)doubleTapped {
    PictureView *picture = _pictureViews[_currentIndex];
    [picture doubleTappedOnPicture];
}
/**
 *  @author carefree
 *
 *  @brief  隐藏
 */
- (void)hide {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        //执行回调
        if (_dismissBlock) {
            _dismissBlock();
        }
    }];
}


@end
