//
//  ViewController.m
//  pictureBrowser
//
//  Created by moliya on 15/3/25.
//  Copyright (c) 2015年 Wanquan Network Technology. All rights reserved.
//

#import "ViewController.h"
#import "MOPictureBrowser.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)show:(id)sender{
    MOPictureBrowser *browser = [[MOPictureBrowser alloc] init];
    browser.frame = self.view.frame;
    [browser addPictures:@[[UIImage imageNamed:@"1.jpg"],[UIImage imageNamed:@"2.jpg"],[UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"],[UIImage imageNamed:@"5.jpg"]]];
    browser.currentIndex = 1;
    [browser showFromView:sender deleteBlock:^(NSUInteger index) {
        NSLog(@"%@",@(index));
    } dismissBlock:^{
        NSLog(@"dismiss");
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
